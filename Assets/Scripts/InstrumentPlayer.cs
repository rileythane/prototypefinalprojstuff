﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentPlayer : MonoBehaviour
{
    AudioSource src;
    public AudioClip C, D, E, G, A, C_;
    public AudioClip CLow, DLow, ELow, GLow, ALow, CLow_;
    public bool isHeld;
    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Joystick1Button6))
        {
            isHeld = true;
        }

        if (Input.GetKeyUp(KeyCode.Joystick1Button6))
        {
            isHeld = false;
        }


        if (isHeld == true)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                src.PlayOneShot(CLow, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                src.PlayOneShot(DLow, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button2))
            {
                src.PlayOneShot(ELow, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button3))
            {
                src.PlayOneShot(GLow, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button4))
            {
                src.PlayOneShot(ALow, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button5))
            {
                src.PlayOneShot(CLow_, 1f);
            }

        }

        else if (isHeld == false)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                src.PlayOneShot(C, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                src.PlayOneShot(D, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button2))
            {
                src.PlayOneShot(E, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button3))
            {
                src.PlayOneShot(G, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button4))
            {
                src.PlayOneShot(A, 1f);
            }

            if (Input.GetKeyDown(KeyCode.Joystick1Button5))
            {
                src.PlayOneShot(C_, 1f);
            }
        }

    }
}
