﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Range(0, 2)]
    public float smoothSpeed = 1;
    public float speed = 10;
    Vector3 targetPosition;
    public Rigidbody rb;
    public float spinForce, upForce;

    public bool canWalk = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector3(0, rb.velocity.y, 0);

        float x = Input.GetAxisRaw("Horizontal");
        float z = Input.GetAxisRaw("Vertical");
        targetPosition = new Vector3(x, 0, z) + transform.position;


        if (Input.GetKey(KeyCode.Joystick1Button0))
        {
            transform.Rotate(Vector3.up * spinForce);
        }

        if (Input.GetKey(KeyCode.Joystick1Button1))
        {
            transform.Rotate(Vector3.down * spinForce);
        }

        if (Input.GetKey(KeyCode.Joystick1Button2))
        {
            transform.Rotate(Vector3.up * spinForce);
        }

        if (Input.GetKey(KeyCode.Joystick1Button3))
        {
            transform.Rotate(Vector3.down * spinForce);
        }

        if (Input.GetKey(KeyCode.Joystick1Button4))
        {
            transform.Rotate(Vector3.up * spinForce);
        }

        if (Input.GetKey(KeyCode.Joystick1Button5))
        {
            transform.Rotate(Vector3.down * spinForce);
            rb.AddForce(Vector3.up * upForce, ForceMode.Impulse);
        }

        if (Input.GetKeyUp(KeyCode.Joystick1Button5))
        {
            rb.velocity = Vector3.down * Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        if (canWalk)
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, (speed * Time.deltaTime) * smoothSpeed);
    }
}
