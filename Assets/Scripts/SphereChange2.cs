﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereChange2 : MonoBehaviour
{
    public bool inRange;
    float spinForce;
    Vector3 spinDir, spinLeft, spinRight;
    public Color startColor, inRangeColor, aPress, bPress, aUp, bUp;
    private Renderer rend;
    float aCounter, bCounter;
    public GameObject rangeOrb;
    // Start is called before the first frame update
    void Start()
    {
        spinRight = new Vector3(0, 1, 0);
        spinLeft = new Vector3(0, -1, 0);
        rend = GetComponent<Renderer>();
        spinForce = 2;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inRange = false;
            aCounter = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Spin();


        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            aCounter++;
            bCounter = 0;
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            bCounter++;
            aCounter = 0;
        }

        //if the player is in range of the sphere it changes to the inRange colour
        if (inRange == true)
        {
            rangeOrb.SetActive(true);
            //if player presses a button, the spheres size and spin direction will change
            if (aCounter == 1 && Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                spinForce = 4;
                rend.material.color = aPress;
                transform.localScale = new Vector3(Random.Range(0.5f, 1), Random.Range(0.5f, 1f), Random.Range(0.5f, 1f));
                spinDir = spinLeft;
                Debug.Log(aCounter);
            }

            if (aCounter == 1 && Input.GetKeyUp(KeyCode.Joystick1Button0))
            {
                spinForce = 6;
                rend.material.color = aUp;
                transform.localScale = new Vector3(Random.Range(0.5f, 1.5f), Random.Range(0.5f, 1.5f), Random.Range(0.5f, 1.5f));
                spinDir = spinRight;
            }

            if (aCounter == 2 && Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                spinForce = 8;
                rend.material.color = aPress;
                transform.localScale = new Vector3(Random.Range(0.5f, 2), Random.Range(0.5f, 2f), Random.Range(0.5f, 2f));
                spinDir = spinLeft;
            }

            if (aCounter == 2 && Input.GetKeyUp(KeyCode.Joystick1Button0))
            {
                spinForce = 10;
                rend.material.color = aUp;
                transform.localScale = new Vector3(Random.Range(0.5f, 3), Random.Range(0.5f, 3f), Random.Range(0.5f, 3f));
                spinDir = spinRight;
            }

            if (aCounter == 3 && Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                spinForce = 12;
                rend.material.color = aPress;
                transform.localScale = new Vector3(Random.Range(0.5f, 3.5f), Random.Range(0.5f, 3.5f), Random.Range(0.5f, 3.5f));
                spinDir = spinLeft;
            }

            if (aCounter == 3 && Input.GetKeyUp(KeyCode.Joystick1Button0))
            {
                spinForce = 14;
                rend.material.color = aUp;
                transform.localScale = new Vector3(Random.Range(1.5f, 3.5f), Random.Range(1.5f, 3.5f), Random.Range(1.5f, 3.5f));
                spinDir = spinRight;
                StartCoroutine(SetSize());
                aCounter = 0;
            }

            //if player presses b button, the spheres size and spin direction will change
            if (bCounter == 1 && Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                spinForce = 4;
                rend.material.color = bPress;
                transform.localScale = new Vector3(Random.Range(0.5f, 1), Random.Range(0.5f, 1f), Random.Range(0.5f, 1f));
                spinDir = spinLeft;
                Debug.Log(aCounter);
            }

            if (bCounter == 1 && Input.GetKeyUp(KeyCode.Joystick1Button1))
            {
                spinForce = 6;
                rend.material.color = bUp;
                transform.localScale = new Vector3(Random.Range(0.5f, 1.5f), Random.Range(0.5f, 1.5f), Random.Range(0.5f, 1.5f));
                spinDir = spinRight;
            }

            if (bCounter == 2 && Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                spinForce = 8;
                rend.material.color = bPress;
                transform.localScale = new Vector3(Random.Range(0.5f, 2), Random.Range(0.5f, 2f), Random.Range(0.5f, 2f));
                spinDir = spinLeft;
            }

            if (bCounter == 2 && Input.GetKeyUp(KeyCode.Joystick1Button1))
            {
                spinForce = 10;
                rend.material.color = bUp;
                transform.localScale = new Vector3(Random.Range(0.5f, 3), Random.Range(0.5f, 3f), Random.Range(0.5f, 3f));
                spinDir = spinRight;
            }

            if (bCounter == 3 && Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                spinForce = 12;
                rend.material.color = bPress;
                transform.localScale = new Vector3(Random.Range(0.5f, 3.5f), Random.Range(0.5f, 3.5f), Random.Range(0.5f, 3.5f));
                spinDir = spinLeft;
            }

            if (bCounter == 3 && Input.GetKeyUp(KeyCode.Joystick1Button1))
            {
                spinForce = 14;
                rend.material.color = bUp;
                transform.localScale = new Vector3(Random.Range(1.5f, 3.5f), Random.Range(1.5f, 3.5f), Random.Range(1.5f, 3.5f));
                spinDir = spinRight;
                StartCoroutine(SetSize());
                bCounter = 0;
            }


        }

        if (inRange == false)
        {
            rangeOrb.SetActive(false);
            aCounter = 0;
            bCounter = 0;
            transform.localScale = new Vector3(1, 1, 1);
        }
    }

    IEnumerator SetSize()
    {
        yield return new WaitForSeconds(1f);
        transform.localScale = new Vector3(1, 1, 1);
        rend.material.color = startColor;
    }


    void GetSmall()
    {
        if (inRange == true)
        {
            transform.localScale -= new Vector3(0.003f, 0.003f, 0.003f);
        }
    }



    void Spin()
    {
        transform.Rotate(spinDir * spinForce);
    }

}
