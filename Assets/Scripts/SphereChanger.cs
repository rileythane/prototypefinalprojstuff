﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereChanger : MonoBehaviour
{
    public bool isHeld, inRange;
    public float spinForce, pressCount;
    public GameObject controller, darkImage;
    Vector3 spinDir, spinLeft, spinRight;
    public Color startColor, inRangeColor;
    private Renderer rend;
    public GameObject rangeOrb;


    // Start is called before the first frame update
    void Start()
    {
        spinRight = new Vector3(0, 1, 0);
        spinLeft = new Vector3(0, -1, 0);
        pressCount = 0f;
        rend = GetComponent<Renderer>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inRange = true;
            controller.transform.localScale = new Vector3(1.15f, 1.15f, 1.15f);
            darkImage.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inRange = false;
            controller.transform.localScale = new Vector3(1f, 1f, 1f);
            darkImage.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Spin();

        if (inRange == true)
        {
            rend.material.color = inRangeColor;
            rangeOrb.SetActive(true);

            if (Input.GetKey(KeyCode.Joystick1Button6))
            {
                isHeld = true;
            }

            if (Input.GetKeyUp(KeyCode.Joystick1Button6))
            {
                isHeld = false;
            }


            if (isHeld == false)
            {
                if (Input.GetKeyDown(KeyCode.Joystick1Button0))
                {
                    transform.localScale = new Vector3(Random.Range(0.5f, 1), Random.Range(0.5f, 1f), Random.Range(0.5f, 1f));
                    spinDir = spinLeft;
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button1))
                {
                    transform.localScale = new Vector3(Random.Range(0.5f, 1.5f), Random.Range(0.5f, 1.5f), Random.Range(0.5f, 1.5f));
                    spinDir = spinRight;
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button2))
                {
                    transform.localScale = new Vector3(Random.Range(0.5f, 2), Random.Range(0.5f, 2f), Random.Range(0.5f, 2f));
                    spinDir = spinLeft;
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button3))
                {
                    transform.localScale = new Vector3(Random.Range(0.5f, 3), Random.Range(0.5f, 3f), Random.Range(0.5f, 3f));
                    spinDir = spinRight;
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button4))
                {
                    transform.localScale = new Vector3(Random.Range(0.5f, 3.5f), Random.Range(0.5f, 3.5f), Random.Range(0.5f, 3.5f));
                    spinDir = spinLeft;
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button5))
                {
                    transform.localScale = new Vector3(Random.Range(1.5f, 3.5f), Random.Range(1.5f, 3.5f), Random.Range(1.5f, 3.5f));
                    spinDir = spinRight;
                }

            }

            else if (isHeld == true)
            {
                if (Input.GetKeyDown(KeyCode.Joystick1Button0))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button1))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button2))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button3))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button4))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }

                if (Input.GetKeyDown(KeyCode.Joystick1Button5))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }


        if (inRange == false)
        {
            rangeOrb.SetActive(false);
            rend.material.color = startColor;
            transform.localScale = new Vector3(1, 1, 1);
        }
    }


    void GetSmall()
    {
        if (inRange == true)
        {
            transform.localScale -= new Vector3(0.003f, 0.003f, 0.003f);
        }
    }



    void Spin()
    {
        transform.Rotate(spinDir * spinForce);
    }

}
