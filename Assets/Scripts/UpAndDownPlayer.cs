﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpAndDownPlayer : MonoBehaviour
{
    AudioSource src;
    public AudioClip C, D, E, G, A, C_;
    public AudioClip CLow, DLow, ELow, GLow, ALow, CLow_;
    public bool isHeld;
    float aCounter, bCounter;
    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(aCounter);

        //this checks how many times a button is pressed to move to the next note in the scale. Resets when it reaches the end of the scale.
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            aCounter++;
            bCounter = 0;
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            bCounter++;
            aCounter = 0;
        }


        //if the player is pressing a (this is the higher scale)
        if (aCounter == 1 && Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            src.PlayOneShot(CLow, 1f);
        }

        if (aCounter == 1 && Input.GetKeyUp(KeyCode.Joystick1Button0))
        {
            src.PlayOneShot(DLow, 1f);
        }

        if (aCounter == 2 && Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            src.PlayOneShot(ELow, 1f);
        }

        if (aCounter == 2 && Input.GetKeyUp(KeyCode.Joystick1Button0))
        {
            src.PlayOneShot(GLow, 1f);
        }

        if (aCounter == 3 && Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            src.PlayOneShot(ALow, 1f);
        }

        if (aCounter == 3 && Input.GetKeyUp(KeyCode.Joystick1Button0))
        {
            src.PlayOneShot(CLow_, 1f);
            aCounter = 0;
        }

        //if the player presses b (this is the lower scale)
        if (bCounter == 1 && Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            src.PlayOneShot(C, 1f);
        }

        if (bCounter == 1 && Input.GetKeyUp(KeyCode.Joystick1Button1))
        {
            src.PlayOneShot(D, 1f);
        }

        if (bCounter == 2 && Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            src.PlayOneShot(E, 1f);
        }

        if (bCounter == 2 && Input.GetKeyUp(KeyCode.Joystick1Button1))
        {
            src.PlayOneShot(G, 1f);
        }

        if (bCounter == 3 && Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            src.PlayOneShot(A, 1f);
        }

        if (bCounter == 3 && Input.GetKeyUp(KeyCode.Joystick1Button1))
        {
            src.PlayOneShot(C_, 1f);
            bCounter = 0;
        }

    }
}
